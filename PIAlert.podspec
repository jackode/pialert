Pod::Spec.new do |s|

# Root specification

  s.name         = "PIAlert"
  s.version      = "0.0.1"
  s.summary      = "Alert Manager"
  s.homepage     = "https://github.com/phamquy/PIAlert"
  s.author       = { "Jack" => "psyquy@gmail.com" }
  s.source       = {:git => 'https://github.com/phamquy/PIAlert.git', :tag=> '0.1'}
  s.license      = 'MIT'
# Build setting
  s.ios.deployment_target = '7.0'
  s.platform = :ios, '7.0' 
  s.framework  = 'UIKit'
  s.requires_arc = true
  s.public_header_files = 'PIAlert/*.h'
  s.source_files  = 'PIAlert/**/*.{h,m}'
  s.ios.resource_bundles = {
    'PIAlert' => [
      'PIAlert/*.lproj'
    ]
  }
  
end