//
//  ViewController.m
//  PIAlertExamples
//
//  Created by Pham Quy on 2/12/15.
//  Copyright (c) 2015 Jkorp. All rights reserved.
//

#import "ViewController.h"
#import <PIAlert/PIAlert.h>
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)alertWithCancel:(id)sender {
//    [UIAlertView
//     alertViewWithTitle:@"Title"
//     message:@"message"
//     cancelButtonTitle:@"cancels"
//     otherButtonTitles:@[@"Btn1", @"Btn2"]
//     onDismiss:^(NSInteger idx) {
//        NSLog(@"%ld", idx);
//    } onCancel:^{
//        NSLog(@"canceled");
//    }];
    
    
    PIAlertAction* action = [[PIAlertAction alloc] initWithTitle:@"btn1" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn1");
    }];
    
    PIAlertAction* action2 = [[PIAlertAction alloc] initWithTitle:@"btn2" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn2");
    }];

    PIAlertAction* action3 = [[PIAlertAction alloc] initWithTitle:@"btn3" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn3");
    }];
    [PIAlertManager alertWithTitle:@"Title" message:@"Message" actions:@[action, action2, action3]];
}

- (IBAction)alertWithoutCancel:(id)sender {
    [UIAlertView
     alertViewWithTitle:@"Title"
     message:@"message"
     cancelButtonTitle:nil
     otherButtonTitles:@[@"Btn1", @"Btn2"]
     onDismiss:^(NSInteger idx) {
        NSLog(@"%ld", (long)idx);
    } onCancel:^{
        NSLog(@"canceled");
    }];
}

- (IBAction)actionSheetWithCancelAndDestructiv:(id)sender {
    [UIActionSheet
     actionSheetWithTitle:@"Title"
     cancelButtonTitle:@"Can"
     cancelBlock:^{
         NSLog(@"Canceld");
     }
     otherButtonTitles:@[@"btn1", @"btn2" ,@"btn3", @"btn4"]
     destructiveIndex:2
     
     dismissBlock:^(NSInteger idx) {
         NSLog(@"%@",@(idx));
    }
     fromView:self.view fromRect:CGRectZero animated:NO];
}


- (IBAction)actionSheetWithCancel:(id)sender {
    PIAlertAction* action = [[PIAlertAction alloc] initWithTitle:@"btn1" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn1");
    }];
    
    PIAlertAction* action2 = [[PIAlertAction alloc] initWithTitle:@"btn2" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn2");
    }];
    
    PIAlertAction* action3 = [[PIAlertAction alloc] initWithTitle:@"btn3" style:(PIAlertActionStyleDestructive) action:^(PIAlertAction *action) {
        NSLog(@"Btn3");
    }];
    
    PIAlertAction* action4 = [[PIAlertAction alloc] initWithTitle:nil style:(PIAlertActionStyleCancel) action:^(PIAlertAction *action) {
        NSLog(@"cancel");
    }];
    [PIAlertManager actionSheetWithTitle:@"title"  actions:@[action, action2, action3, action4] fromView:self.view fromRect:CGRectZero animated:YES];
}


- (IBAction)actionSheetWithDestructive:(id)sender {
}


- (IBAction)actionSheetWithButton:(id)sender {
}

@end
