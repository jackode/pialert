//
//  UIAlertView+PIAlert.h
//  Pods
//
//  Created by Pham Quy on 2/12/15.
//
//

#import <UIKit/UIKit.h>

@interface UIAlertView (PIAlert)
+ (void) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message;

+ (void) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message
                  cancelButtonTitle:(NSString*) cancelButtonTitle
                           onCancel:(void(^)(void)) cancelBlock;

+ (void) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message
                  cancelButtonTitle:(NSString*) cancelButtonTitle
                  otherButtonTitles:(NSArray*) buttonTitles
                          onDismiss:(void(^)(NSInteger)) dimissBlock
                           onCancel:(void(^)(void)) cancelBlock;
@end
