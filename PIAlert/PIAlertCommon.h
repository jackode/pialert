//
//  PIAlertCommon.h
//  Pods
//
//  Created by Pham Quy on 2/12/15.
//
//

#ifndef Pods_PIAlertCommon_h
#define Pods_PIAlertCommon_h

#import <objc/runtime.h>
#define SYNTHESIZE_CATEGORY_OBJ_PROPERTY(propertyGetter, propertySetter)                                                             \
- (id) propertyGetter {                                                                                                             \
return objc_getAssociatedObject(self, @selector( propertyGetter ));                                                             \
}                                                                                                                                   \
- (void) propertySetter (id)obj{                                                                                                    \
objc_setAssociatedObject(self, @selector( propertyGetter ), obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);                            \
}


#define SYNTHESIZE_CATEGORY_VALUE_PROPERTY(valueType, propertyGetter, propertySetter)                                                \
- (valueType) propertyGetter {                                                                                                      \
valueType ret = {0};                                                                                                                  \
[objc_getAssociatedObject(self, @selector( propertyGetter )) getValue:&ret];                                                    \
return ret;                                                                                                                     \
}                                                                                                                                   \
- (void) propertySetter (valueType)value{                                                                                           \
NSValue *valueObj = [NSValue valueWithBytes:&value objCType:@encode(valueType)];                                                \
objc_setAssociatedObject(self, @selector( propertyGetter ), valueObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);                       \
}
#endif


#define PIA_SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define PIA_SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define PIA_SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define PIA_SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define PIA_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define PIA_SAFE_CALL(block, ...) if((block)) { block(__VA_ARGS__);}
#define PIA_SAFE_OBJ_AT(_array_, _index_) (((_array_) && ([(_array_) count] > (_index_)))? (_array_)[(_index_)] : nil)

#define PIA_LOCALIZE_TABLE @"PIAlertString"
#define PIA_LOCALIZE_STR(_KEY_) NSLocalizedStringFromTableInBundle((_KEY_), PIA_LOCALIZE_TABLE, \
[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"PIAlert" ofType:@"bundle"]], nil)
