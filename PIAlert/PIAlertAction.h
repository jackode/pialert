//
//  PIAlertAction.h
//  NewPiki
//
//  Created by Pham Quy on 2/11/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, PIAlertActionStyle)
{
    PIAlertActionStyleDefault,
    PIAlertActionStyleCancel,
    PIAlertActionStyleDestructive
};

@interface PIAlertAction : NSObject
@property (nonatomic, strong) NSString* title;
@property (nonatomic) PIAlertActionStyle style;
@property (nonatomic, readonly) UIAlertAction* uiAlertAction;
@property (nonatomic, copy) void (^action)(PIAlertAction* action);

- (instancetype) initWithTitle:(NSString*) title
                         style:(PIAlertActionStyle)style
                        action:(void (^)(PIAlertAction* action)) action;

@end
