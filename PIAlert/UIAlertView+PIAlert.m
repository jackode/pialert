//
//  UIAlertView+PIAlert.m
//  Pods
//
//  Created by Pham Quy on 2/12/15.
//
//

#import "PIAlertCommon.h"
#import "UIAlertView+PIAlert.h"



@interface _PIAlertViewInternalBlockController : NSObject <UIAlertViewDelegate>
@property (nonatomic, copy) void(^dismissBlock)(NSInteger);
@property (nonatomic, copy) void(^cancelBlock)();
@end


@implementation _PIAlertViewInternalBlockController

- (void)alertView:(UIAlertView*) alertView didDismissWithButtonIndex:(NSInteger) buttonIndex {
    
    if(buttonIndex == [alertView cancelButtonIndex])
    {
        if (self.cancelBlock) {
            self.cancelBlock();
        }
    }
    else
    {
        if (self.dismissBlock) {
            self.dismissBlock(buttonIndex - (alertView.cancelButtonIndex < 0 ? 0 : 1));
        }
    }
}
@end


@interface UIAlertView ()
@property (nonatomic, strong) _PIAlertViewInternalBlockController* piAlertViewInternalBlockController;
@end

@implementation UIAlertView (PIAlert)
SYNTHESIZE_CATEGORY_OBJ_PROPERTY(piAlertViewInternalBlockController, setPiAlertViewInternalBlockController:)

+ (void) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message
{
     [self alertViewWithTitle:title
                     message:message
           cancelButtonTitle:NSLocalizedStringFromTable(@"PIA_CANCEL", @"PIAlertString", @"")
                    onCancel:nil];
}

+ (void) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message
                  cancelButtonTitle:(NSString*) cancelButtonTitle
                           onCancel:(void(^)(void)) cancelBlock
{
     [self alertViewWithTitle:title message:message cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil onDismiss:nil onCancel:cancelBlock];
}

//------------------------------------------------------------------------------
+ (void) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message
                  cancelButtonTitle:(NSString*) cancelButtonTitle
                  otherButtonTitles:(NSArray*) buttonTitles
                          onDismiss:(void(^)(NSInteger)) dimissBlock
                           onCancel:(void(^)(void)) cancelBlock
{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:cancelButtonTitle
                                          otherButtonTitles:nil];
    
    _PIAlertViewInternalBlockController* alertControl = [[_PIAlertViewInternalBlockController alloc] init];
    [alertControl setDismissBlock:dimissBlock];
    [alertControl setCancelBlock:cancelBlock];
    
    [alertView setPiAlertViewInternalBlockController:alertControl];
    [alertView setDelegate:alertControl];
    
    for(NSString *buttonTitle in buttonTitles){
        [alertView addButtonWithTitle:buttonTitle];
    }
    
    
    [alertView show];
}
@end
