//
//  PIAlertManager.m
//  NewPiki
//
//  Created by Pham Quy on 2/11/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import "PIAlertCommon.h"

#import "PIAlertManager.h"
#import "PIAlertAction.h"
#import "UIViewController+PIUtil.h"
#import "UIAlertView+PIAlert.h"
#import "UIActionSheet+PIAlert.h"

//@interface PIAlertManager ()
//@property (nonatomic) NSArray* otherActions;
//@end


@implementation PIAlertManager
+ (void) alertWithTitle:(NSString*)title
           message:(NSString*)message
{
    [self alertWithTitle:title message:message actions:nil style:PIAlertStyleAlert fromView:nil viewController:nil showCompletion:nil];
}

//------------------------------------------------------------------------------
+ (void) alertWithTitle:(NSString*)title
           message:(NSString*)message
           actions:(NSArray*)actions
{
    [self alertWithTitle:title message:message actions:actions style:PIAlertStyleAlert fromView:nil viewController:nil showCompletion:nil];
}

//------------------------------------------------------------------------------
+ (void) alertWithTitle:(NSString*)title
           message:(NSString*)message
           actions:(NSArray*)actions
fromViewController:(UIViewController*) viewController
{
    [self alertWithTitle:title message:message actions:actions style:PIAlertStyleAlert fromView:nil viewController:viewController showCompletion:nil];
}
//------------------------------------------------------------------------------
+ (void) actionSheetWithTitle:(NSString*)title
                      actions:(NSArray*)actions
                     fromView:(UIView*)view
                     fromRect:(CGRect) rect
                     animated:(BOOL) animated
{
    [self alertWithTitle:title message:nil actions:actions style:PIAlertStyleActionSheet  fromView:view viewController:nil showCompletion:nil];
}
//------------------------------------------------------------------------------
+ (void) alertWithTitle:(NSString*)title
           message:(NSString*)message
           actions:(NSArray*)actions
             style:(PIAlertStyle) style
          fromView:(UIView*) view
    viewController:(UIViewController*) viewController
    showCompletion:(void(^)(void)) completion

{
    if (PIA_SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [self ios8_alertWithTitle:title message:message actions:actions style:style viewController:viewController showCompletion:completion];
    }else{
        [self ios7_alertWithTitle:title message:message actions:actions style:style fromView:view];
    }
}

//------------------------------------------------------------------------------
+ (void) ios8_alertWithTitle:(NSString*)title
           message:(NSString*)message
           actions:(NSArray*)actions
             style:(PIAlertStyle) style
         viewController:(UIViewController*) viewController
         showCompletion:(void(^)(void)) completion
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:[self alertViewControllerStyleForStyle:style]];
    
    
    if (actions && actions.count) {
        for (PIAlertAction* action in actions) {
            UIAlertAction* uiAlertAction = [action uiAlertAction];
            [alertController addAction:uiAlertAction];
        }
    }else{
        PIAlertAction* action = [[PIAlertAction alloc] init];
        [action setTitle:PIA_LOCALIZE_STR(@"PIA_CANCEL")];
        [alertController addAction:[action uiAlertAction]];
    }
    
    UIViewController* presenter = viewController ? viewController : [UIViewController presenter];

    [presenter
     presentViewController:alertController
     animated:YES
     completion:completion];
}

//------------------------------------------------------------------------------
+ (void) ios7_alertWithTitle:(NSString*)title
                message:(NSString*)message
                actions:(NSArray*)actions
                  style:(PIAlertStyle) style
               fromView:(UIView*) view
{
    

    
    if (style == PIAlertStyleAlert) {
        // Find cancel action
        PIAlertAction* cancelAction = nil;
        NSMutableArray* otherActions = [NSMutableArray array];
        NSMutableArray* titles = [NSMutableArray array];
        NSString* cancelTitle = nil;
        
        
        if (actions  && actions.count) {
            for (PIAlertAction* action in actions) {
                if (action.style == PIAlertActionStyleCancel) {
                    cancelAction = action;
                }else{
                    [otherActions addObject:action];
                }
            }
            
            
            for (PIAlertAction* action in otherActions) {
                [titles addObject:(action.title?action.title: @"")];
            }
            
            if (!titles.count) {
                titles = nil;
            }
            
            cancelTitle = cancelAction ? (cancelAction.title ? cancelAction.title : PIA_LOCALIZE_STR(@"PIA_CANCEL")) : nil;

        }else{
            cancelTitle =  PIA_LOCALIZE_STR(@"PIA_CANCEL");
            titles = nil;
        }
        

        
        [UIAlertView
         alertViewWithTitle:title
         message:message
         cancelButtonTitle:cancelTitle
         otherButtonTitles:titles
         onDismiss:^(NSInteger buttonIndex) {
            PIAlertAction* action = PIA_SAFE_OBJ_AT(otherActions, buttonIndex);
            PIA_SAFE_CALL(action.action, action);
            
        } onCancel:^{
            PIA_SAFE_CALL(cancelAction.action, cancelAction);
        }];
    }else{
        // Find cancel action
        PIAlertAction* destructAction = nil;
        PIAlertAction* cancelAction = nil;
        NSMutableArray* titles = [NSMutableArray array];
        NSMutableArray* otherActions = [NSMutableArray array];
        NSString* cancelTitle = nil;
        
        if (actions && actions.count) {
            for (PIAlertAction* action in actions) {
                if (action.style == PIAlertActionStyleCancel){
                    cancelAction = action;
                }else{
                    if (action.style == PIAlertActionStyleDestructive) {
                        destructAction= action;
                    }
                    [titles addObject:(action.title?action.title: @"")];
                    [otherActions addObject:action];
                }
            }
            
            if (!titles.count) {
                titles = nil;
            }
            
            
            cancelTitle = cancelAction? (cancelAction.title ? cancelAction.title : PIA_LOCALIZE_STR(@"PIA_CANCEL")) : nil;
        }else {
            cancelTitle = PIA_LOCALIZE_STR(@"PIA_CANCEL");
            titles=  nil;
        }
        
        NSInteger dIdx = destructAction ? [otherActions indexOfObject:destructAction] : -1;
        
        [UIActionSheet
         actionSheetWithTitle:title
         cancelButtonTitle:cancelTitle
         cancelBlock:^{
             PIA_SAFE_CALL(cancelAction.action, cancelAction);
         }
         otherButtonTitles:titles
         destructiveIndex:dIdx
         dismissBlock:^(NSInteger idx) {
             PIAlertAction* action = PIA_SAFE_OBJ_AT(otherActions, idx);
             PIA_SAFE_CALL(action.action, action);
         }
         fromView:view
         fromRect:CGRectZero
         animated:YES];
    }
}

+ (UIAlertControllerStyle) alertViewControllerStyleForStyle:(PIAlertStyle) style
{
    switch (style) {
        case PIAlertStyleAlert:
            return UIAlertControllerStyleAlert;
            break;
        case PIAlertStyleActionSheet:
            return UIAlertControllerStyleActionSheet;
        default:
            break;
    }
    return UIAlertControllerStyleAlert;
}
@end
