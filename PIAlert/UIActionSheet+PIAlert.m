//
//  UIActionSheet+PIAlert.m
//  Pods
//
//  Created by Pham Quy on 2/12/15.
//
//
#import "PIAlertCommon.h"
#import "UIActionSheet+PIAlert.h"

@interface _PIActionSheetInternalBlockController : NSObject <UIActionSheetDelegate>
@property (nonatomic, copy) void(^dismissBlock)(NSInteger);
@property (nonatomic, copy) void(^cancelBlock)();
@end


@implementation _PIActionSheetInternalBlockController

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == [actionSheet cancelButtonIndex])
    {
        if (self.cancelBlock) {
            self.cancelBlock();
        }
    }else{

        self.dismissBlock(buttonIndex);
    }
}
@end


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

@interface UIActionSheet()
@property (nonatomic, strong) _PIActionSheetInternalBlockController* piActionSheetInternalBlockController;
@end


@implementation UIActionSheet (PIAlert)
SYNTHESIZE_CATEGORY_OBJ_PROPERTY(piActionSheetInternalBlockController, setPiActionSheetInternalBlockController:)


+ (void) actionSheetWithTitle:(NSString*) title
            otherButtonTitles:(NSArray*) buttonTitles
                 dismissBlock:(void(^)(NSInteger)) dismissBlock
                     fromView:(UIView*) view
{
    [self actionSheetWithTitle:title cancelButtonTitle:nil cancelBlock:nil otherButtonTitles:buttonTitles destructiveIndex:-1  dismissBlock:dismissBlock fromView:view fromRect:CGRectZero animated:YES];
}

+ (void) actionSheetWithTitle:(NSString*) title
            otherButtonTitles:(NSArray*) buttonTitles
                 dismissBlock:(void(^)(NSInteger)) dismissBlock
                     fromView:(UIView*) view
                     fromRect:(CGRect) rect
                     animated:(BOOL)animated
{
    [self actionSheetWithTitle:title cancelButtonTitle:nil  cancelBlock:nil   otherButtonTitles:buttonTitles destructiveIndex:-1  dismissBlock:dismissBlock fromView:view fromRect:rect animated:animated];
}


+ (void) actionSheetWithTitle:(NSString*) title
            cancelButtonTitle:(NSString*) cancelTitle
                  cancelBlock:(void(^)()) cancelBlock
            otherButtonTitles:(NSArray*) buttonTitles
                 dismissBlock:(void(^)(NSInteger)) dismissBlock
                     fromView:(UIView*) view
                     fromRect:(CGRect) rect
                     animated:(BOOL)animated
{
    [self actionSheetWithTitle:title cancelButtonTitle:cancelBlock cancelBlock:cancelBlock otherButtonTitles:buttonTitles destructiveIndex:-1 dismissBlock:dismissBlock fromView:view fromRect:rect animated:animated];
}

+ (void) actionSheetWithTitle:(NSString*) title
            cancelButtonTitle:(NSString*) cancelTitle
                  cancelBlock:(void(^)()) cancelBlock
            otherButtonTitles:(NSArray*) buttonTitles
             destructiveIndex:(NSInteger) index
                 dismissBlock:(void(^)(NSInteger)) dismissBlock
                     fromView:(UIView*) view
                     fromRect:(CGRect) rect
                     animated:(BOOL)animated
{
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    _PIActionSheetInternalBlockController* controller = [[_PIActionSheetInternalBlockController alloc] init];
    [controller setCancelBlock:cancelBlock];
    [controller setDismissBlock:dismissBlock];
    
    [actionSheet setPiActionSheetInternalBlockController:controller];
    [actionSheet setDelegate:controller];
    
    NSArray* allTitles = [buttonTitles arrayByAddingObject:cancelTitle];
    for(NSString *buttonTitle in allTitles){
        [actionSheet addButtonWithTitle:buttonTitle];
    }
    

    actionSheet.destructiveButtonIndex = (buttonTitles && (buttonTitles.count > index) && (index >= 0)) ? (index) : -1;
    actionSheet.cancelButtonIndex = allTitles.count-1;
    
    if([view isKindOfClass:[UITabBar class]])
        [actionSheet showFromTabBar:(UITabBar*) view];
    
    if([view isKindOfClass:[UIBarButtonItem class]])
        [actionSheet showFromBarButtonItem:(UIBarButtonItem*) view animated:animated];

    if([view isKindOfClass:[UIView class]])
    {
        if (CGRectIsEmpty(rect)) {
            [actionSheet showInView:view];
        }else{
            [actionSheet showFromRect:rect inView:view animated:animated];
        }
    }
}
@end
