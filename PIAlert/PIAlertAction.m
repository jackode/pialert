//
//  PIAlertAction.m
//  NewPiki
//
//  Created by Pham Quy on 2/11/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import <PIAlertCommon.h>
#import "PIAlertAction.h"
@interface PIAlertAction ()
@property (nonatomic, strong) UIAlertAction* uiAlertAction;
@end

@implementation PIAlertAction

- (instancetype) initWithTitle:(NSString*) title
                         style:(PIAlertActionStyle)style
                        action:(void (^)(PIAlertAction* action)) action
{
    self = [super init];
    if (self) {
        self.title = title;
        self.action = action;
        self.style = style;
    }
    return self;
    
}
//------------------------------------------------------------------------------
- (UIAlertAction*) uiAlertAction
{
    if (!_uiAlertAction) {
        NSString* title = self.title ? self.title : (self.style == PIAlertActionStyleCancel ? PIA_LOCALIZE_STR(@"PIA_CANCEL") : @"");
        __weak typeof(self) wself = self;
        UIAlertAction *uiaction =
        [UIAlertAction
         actionWithTitle:title
         style:[PIAlertAction alertActionStyleFromStyle:self.style]
         handler:^(UIAlertAction *action) {
             if (self.action) {
                 self.action(wself);
             }
         }];
        _uiAlertAction = uiaction;
    }
    
    return _uiAlertAction;
}

//------------------------------------------------------------------------------
+ (UIAlertActionStyle) alertActionStyleFromStyle:(PIAlertActionStyle) style
{
    switch (style) {
        case PIAlertActionStyleDefault:
            return UIAlertActionStyleDefault;
            break;
        case PIAlertActionStyleCancel:
            return UIAlertActionStyleCancel;
            break;
        case PIAlertActionStyleDestructive:
            return UIAlertActionStyleDestructive;
            break;
        default:
            break;
    }
    
    return UIAlertActionStyleDefault;
}
@end
