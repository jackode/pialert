//
//  UIActionSheet+PIAlert.h
//  Pods
//
//  Created by Pham Quy on 2/12/15.
//
//

#import <UIKit/UIKit.h>

@interface UIActionSheet (PIAlert)

+ (void) actionSheetWithTitle:(NSString*) title
            otherButtonTitles:(NSArray*) buttonTitles
                 dismissBlock:(void(^)(NSInteger)) dismissBlock
                     fromView:(UIView*) view;

+ (void) actionSheetWithTitle:(NSString*) title
            otherButtonTitles:(NSArray*) buttonTitles
                 dismissBlock:(void(^)(NSInteger)) dismissBlock
                     fromView:(UIView*) view
                     fromRect:(CGRect) rect
                     animated:(BOOL)animated;


+ (void) actionSheetWithTitle:(NSString*) title
            cancelButtonTitle:(NSString*) cancelTitle
                  cancelBlock:(void(^)()) cancelBlock
            otherButtonTitles:(NSArray*) buttonTitles
                 dismissBlock:(void(^)(NSInteger)) dismissBlock
                     fromView:(UIView*) view
                     fromRect:(CGRect) rect
                     animated:(BOOL)animated;


+ (void) actionSheetWithTitle:(NSString*) title
            cancelButtonTitle:(NSString*) cancelTitle
                  cancelBlock:(void(^)()) cancelBlock
            otherButtonTitles:(NSArray*) buttonTitles
             destructiveIndex:(NSInteger) index
                 dismissBlock:(void(^)(NSInteger)) dismissBlock
                     fromView:(UIView*) view
                     fromRect:(CGRect) rect
                     animated:(BOOL)animated;

@end
