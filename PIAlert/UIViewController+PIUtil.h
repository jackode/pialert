//
//  UIViewController+PIUtil.h
//  NewPiki
//
//  Created by Pham Quy on 2/10/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PIUtil)
+ (UIViewController*) presenter;
+ (UIViewController*) recursiveSearch:(UIViewController*) rootVc;
@end
