//
//  UIViewController+PIUtil.m
//  NewPiki
//
//  Created by Pham Quy on 2/10/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import "UIViewController+PIUtil.h"

@implementation UIViewController (PIUtil)
+ (UIViewController*) presenter
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    UIViewController *rootViewController = window.rootViewController;
    
    UIViewController* presenter = [self recursiveSearch:rootViewController];
    if (!presenter) {
        return rootViewController;
    }else{
        return presenter;
    }
}

+ (UIViewController*) recursiveSearch:(UIViewController*) rootVc
{
    UIViewController* result = nil;
    NSArray* childViewControllers = rootVc.childViewControllers;
    if (!rootVc.presentedViewController && !rootVc.childViewControllers.count) {
        result = rootVc;
    }
    else if (rootVc.presentedViewController)
    {
        result = [self recursiveSearch:rootVc.presentedViewController];
    }else{
        for (UIViewController* cVc in childViewControllers) {
            UIViewController* foundVc = [self recursiveSearch:cVc];
            if (foundVc) {
                result = foundVc;
                break;
            }
        }
    }
    return result;
}

@end
