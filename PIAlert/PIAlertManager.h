//
//  PIAlertManager.h
//  NewPiki
//
//  Created by Pham Quy on 2/11/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PIAlertAction.h"

typedef NS_ENUM(NSInteger, PIAlertStyle)
{
    PIAlertStyleActionSheet,
    PIAlertStyleAlert,
    PIAlertStyleDefault=PIAlertStyleActionSheet
};

@interface PIAlertManager : NSObject
+ (void) alertWithTitle:(NSString*)title
           message:(NSString*)message;

+ (void) alertWithTitle:(NSString*)title
           message:(NSString*)message
           actions:(NSArray*)actions;

+ (void) alertWithTitle:(NSString*)title
           message:(NSString*)message
           actions:(NSArray*)actions
fromViewController:(UIViewController*) viewController;

+ (void) actionSheetWithTitle:(NSString*)title
                      actions:(NSArray*)actions
                     fromView:(UIView*)view
                     fromRect:(CGRect) rect
                     animated:(BOOL) animated;

@end
