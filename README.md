# PIAlert
- Provide simple infterface to show alert view and action sheet.
- Support both UIAlertView and UIAlertController
- Block base

# Example


Use `UIAlertView` directly
```
    [UIAlertView
     alertViewWithTitle:@"Title"
     message:@"message"
     cancelButtonTitle:@"cancel"
     otherButtonTitles:@[@"Btn1", @"Btn2"]
     onDismiss:^(NSInteger idx) {
        NSLog(@"%ld", idx);
    } onCancel:^{
        NSLog(@"canceled");
    }];
```

Use `PIAlertManager` to auto switch to UIAlertController in iOS8

```
    PIAlertAction* action = [[PIAlertAction alloc] initWithTitle:@"btn1" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn1");
    }];
    
    PIAlertAction* action2 = [[PIAlertAction alloc] initWithTitle:@"btn2" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn2");
    }];

    PIAlertAction* action3 = [[PIAlertAction alloc] initWithTitle:@"btn3" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn3");
    }];
    [PIAlertManager alertWithTitle:@"Title" message:@"Message" actions:@[action, action2, action3]];
```

Action sheet

```
    [UIActionSheet
     actionSheetWithTitle:@"Title"
     cancelButtonTitle:@"Can"
     cancelBlock:^{
         NSLog(@"Canceld");
     }
     otherButtonTitles:@[@"btn1", @"btn2" ,@"btn3", @"btn4"]
     destructiveIndex:2
     
     dismissBlock:^(NSInteger idx) {
         NSLog(@"%@",@(idx));
    }
     fromView:self.view fromRect:CGRectZero animated:NO];
```
With manager

```
    PIAlertAction* action = [[PIAlertAction alloc] initWithTitle:@"btn1" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn1");
    }];
    
    PIAlertAction* action2 = [[PIAlertAction alloc] initWithTitle:@"btn2" style:(PIAlertActionStyleDefault) action:^(PIAlertAction *action) {
        NSLog(@"Btn2");
    }];
    
    PIAlertAction* action3 = [[PIAlertAction alloc] initWithTitle:@"btn3" style:(PIAlertActionStyleDestructive) action:^(PIAlertAction *action) {
        NSLog(@"Btn3");
    }];
    
    PIAlertAction* action4 = [[PIAlertAction alloc] initWithTitle:nil style:(PIAlertActionStyleCancel) action:^(PIAlertAction *action) {
        NSLog(@"cancel");
    }];
    [PIAlertManager actionSheetWithTitle:@"title"  actions:@[action, action2, action3, action4] fromView:self.view fromRect:CGRectZero animated:YES];
```
